// Copyright 2014, Ali Najafizadeh. All rights reserved.
// Use of this source code is governed by a BSD-style
// Author: Ali Najafizadeh

//Inspired by: https://gist.github.com/bemasher/1777766

package collections

import "fmt"

type item struct {
	value interface{}
	next  *item
}

//Stack the implementation of stack
//this stack is not thread safe!
type Stack struct {
	top  *item
	size int
}

//Len returns the size of stack
func (s *Stack) Len() int {
	return s.size
}

//Push pushs a value to the top of stack
func (s *Stack) Push(value interface{}) {
	s.top = &item{
		value: value,
		next:  s.top,
	}
	s.size++
}

//Pop returns a top value. make sure to check exists
//it is possible to push nil value. So again check the exists value
func (s *Stack) Pop() (value interface{}) {
	if s.size > 0 {
		value, s.top = s.top.value, s.top.next
		s.size--
	}

	return
}

//Peek returns a top without removing it from list
func (s *Stack) Peek() (value interface{}) {
	if s.size > 0 {
		value = s.top.value
	}

	return
}

//PeekFromTop returns element i from the top without removing it
func (s *Stack) PeekFromTop(n int) (value interface{}) {
	if (n >= 0) && (n < s.size) {
		theItem := s.top
		value = theItem.value
		for i := 0; i < n; i++ {
			theItem = theItem.next
			value = theItem.value
		}
	}

	return
}

//PeekFromBottom returns element i from the root without removing it
func (s *Stack) PeekFromBottom(n int) (value interface{}) {
	return s.PeekFromTop(s.size - n - 1)
}

//Slice returns the stack as slice
func (s *Stack) Slice() (out []interface{}) {
	out = make([]interface{}, s.size)
	nextItem := s.top
	for i := s.size - 1; nextItem != nil; i-- {
		out[i] = nextItem.value
		nextItem = nextItem.next
	}
	return
}

// String for implementing interface Stringer
func (s Stack) String() string {
	return fmt.Sprint(s.Slice())
}
