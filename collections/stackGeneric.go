// Copyright 2014, Ali Najafizadeh. All rights reserved.
// Use of this source code is governed by a BSD-style
// Author: Ali Najafizadeh

//Inspired by: https://gist.github.com/bemasher/1777766

package collections

import (
	"fmt"

	"github.com/cheekybits/genny/generic"
)

// go:generate genny -in=$GOFILE -out=gen-$GOFILE gen "Generic=string,int"

// Generic is the type that will be replaced by genny
type Generic generic.Type

type itemGeneric struct {
	value Generic
	next  *itemGeneric
}

//StackGeneric the implementation of stack
//this stack is not thread safe!
type StackGeneric struct {
	top  *itemGeneric
	size int
}

//Len returns the size of stack
func (s *StackGeneric) Len() int {
	return s.size
}

//Push pushs a value to the top of stack
func (s *StackGeneric) Push(value Generic) {
	s.top = &itemGeneric{
		value: value,
		next:  s.top,
	}
	s.size++
}

//Pop returns a top value. make sure to check exists
//it is possible to push nil value. So again check the exists value
func (s *StackGeneric) Pop() (value Generic) {
	if s.size > 0 {
		value, s.top = s.top.value, s.top.next
		s.size--
	}

	return
}

//Peek returns a top without removing it from list
func (s *StackGeneric) Peek() (value Generic) {
	if s.size > 0 {
		value = s.top.value
	}

	return
}

//PeekFromTop returns element i from the top without removing it
func (s *StackGeneric) PeekFromTop(n int) (value Generic) {
	if (n >= 0) && (n < s.size) {
		theItem := s.top
		value = theItem.value
		for i := 0; i < n; i++ {
			theItem = theItem.next
			value = theItem.value
		}
	}

	return
}

//PeekFromBottom returns element i from the root without removing it
func (s *StackGeneric) PeekFromBottom(n int) (value Generic) {
	return s.PeekFromTop(s.size - n - 1)
}

//Slice returns the stack as slice
func (s *StackGeneric) Slice() (out []Generic) {
	out = make([]Generic, s.size)
	nextItem := s.top
	for i := s.size - 1; nextItem != nil; i-- {
		out[i] = nextItem.value
		nextItem = nextItem.next
	}
	return
}

// String for implementing interface Stringer
func (s StackGeneric) String() string {
	return fmt.Sprint(s.Slice())
}
