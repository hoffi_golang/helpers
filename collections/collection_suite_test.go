package collections_test

import (
	"log"
	"testing"

	. "github.com/onsi/ginkgo"
	"github.com/onsi/ginkgo/reporters"
	. "github.com/onsi/gomega"
)

func TestCollectionsPackage(t *testing.T) {
	log.SetFlags(log.LstdFlags | log.Lshortfile)

	RegisterFailHandler(Fail)
	junitReporter := reporters.NewJUnitReporter("junit.xml")
	RunSpecsWithDefaultAndCustomReporters(t, "collections Suite", []Reporter{junitReporter})
}
