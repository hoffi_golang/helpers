package collections_test

import (
	"fmt"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"gitlab.com/hoffi_golang/helpers/collections"
)

var _ = Describe("stackString", func() {

	Describe("nice tests", func() {
		Context("three string elements stack", func() {
			stack := collections.StackString{}
			stack.Push("eins")
			stack.Push("zwei")
			stack.Push("drei")

			It("has 3 entries", func() {
				len := stack.Len()
				Expect(len).To(BeNumerically("==", 3))
			})
			It("Peek is drei", func() {
				s := stack.Peek()
				Expect(s).To(Equal("drei"))
			})
			It("Pop is drei", func() {
				s := stack.Pop()
				Expect(s).To(Equal("drei"))
			})
			It("Peek is zwei", func() {
				s := stack.Peek()
				Expect(s).To(Equal("zwei"))
			})
			It("has 2 entries", func() {
				len := stack.Len()
				Expect(len).To(BeNumerically("==", 2))
			})
			It("after vier pushed has 3 entries", func() {
				stack.Push("vier")
				len := stack.Len()
				Expect(len).To(BeNumerically("==", 3))
			})
			It("and top element is vier", func() {
				s := stack.Peek()
				Expect(s).To(Equal("vier"))
			})
			It("PeekFromTop 0 is vier", func() {
				s := stack.PeekFromTop(0)
				Expect(s).To(Equal("vier"))
			})
			It("PeekFromTop 1 is zwei", func() {
				s := stack.PeekFromTop(1)
				Expect(s).To(Equal("zwei"))
			})
			It("PeekFromBottom 0 is eins", func() {
				s := stack.PeekFromBottom(0)
				Expect(s).To(Equal("eins"))
			})
			It("PeekFromBottom 1 is zwei", func() {
				s := stack.PeekFromBottom(1)
				Expect(s).To(Equal("zwei"))
			})
		})
	})

	Describe("empty stack tests", func() {
		Context("three string elements stack", func() {
			stack := collections.StackString{}

			It("has 0 entries", func() {
				len := stack.Len()
				Expect(len).To(BeNumerically("==", 0))
			})
			It("Peek is nil", func() {
				s := stack.Peek()
				Expect(s).To(Equal(""))
			})
			It("Pop is nil", func() {
				s := stack.Pop()
				Expect(s).To(Equal(""))
			})
			It("Peek is still nil", func() {
				s := stack.Peek()
				Expect(s).To(Equal(""))
			})
			It("Pop is still nil", func() {
				s := stack.Pop()
				Expect(s).To(Equal(""))
			})
			It("PeekFromTop 0 is nil", func() {
				s := stack.PeekFromTop(0)
				Expect(s).To(Equal(""))
			})
			It("PeekFromTop 1 is nil", func() {
				s := stack.PeekFromTop(1)
				Expect(s).To(Equal(""))
			})
			It("PeekFromBottom 0 is nil", func() {
				s := stack.PeekFromBottom(0)
				Expect(s).To(Equal(""))
			})
			It("PeekFromBottom 1 is nil", func() {
				s := stack.PeekFromBottom(1)
				Expect(s).To(Equal(""))
			})
			It("Push eins", func() {
				stack.Push("eins")
				s := stack.Peek()
				Expect(s).To(Equal("eins"))
				Expect(stack.Len()).To(BeNumerically("==", 1))
				s = stack.Pop()
				Expect(s).To(Equal("eins"))
				Expect(stack.Len()).To(BeNumerically("==", 0))
			})
			It("Peek is still still nil", func() {
				s := stack.Peek()
				Expect(s).To(Equal(""))
			})
			It("Pop is still still nil", func() {
				s := stack.Pop()
				Expect(s).To(Equal(""))
			})
		})

		Describe("stack slice tests", func() {
			Context("three string elements stack", func() {
				stack := collections.StackString{}
				stack.Push("eins")
				stack.Push("zwei")
				stack.Push("drei")
				slice := stack.Slice()

				It("slice has 3 entries", func() {
					len := len(slice)
					cap := cap(slice)
					Expect(len).To(BeNumerically("==", 3))
					Expect(cap).To(BeNumerically("==", 3))
				})
				It("slice entries are eins zwei und drei", func() {
					Expect(slice[0]).To(Equal("eins"))
					Expect(slice[1]).To(Equal("zwei"))
					Expect(slice[2]).To(Equal("drei"))
				})
				It("Stringer is using Slice", func() {
					s := fmt.Sprint(slice)
					Expect(s).To(Equal("[eins zwei drei]"))
				})
			})
		})
	})
})
