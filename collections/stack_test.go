package collections_test

import (
	"fmt"

	"gitlab.com/hoffi_golang/helpers/collections"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

var _ = Describe("stack", func() {
	type item struct {
		eins string
		zwei string
	}
	Describe("nice tests", func() {
		Context("three string elements stack", func() {
			stack := collections.Stack{}
			stack.Push(item{"einseins", "einszwei"})
			stack.Push(item{"zweieins", "zweizwei"})
			stack.Push(item{"dreieins", "dreizwei"})

			It("has 3 entries", func() {
				len := stack.Len()
				Expect(len).To(BeNumerically("==", 3))
			})
			It("Peek is drei", func() {
				s := stack.Peek().(item)
				Expect(s).To(Equal(item{"dreieins", "dreizwei"}))
			})
			It("Pop is drei", func() {
				s := stack.Pop().(item)
				Expect(s).To(Equal(item{"dreieins", "dreizwei"}))
			})
			It("Peek is zwei", func() {
				s := stack.Peek()
				Expect(s).To(Equal(item{"zweieins", "zweizwei"}))
			})
			It("has 2 entries", func() {
				len := stack.Len()
				Expect(len).To(BeNumerically("==", 2))
			})
			It("after vier pushed has 3 entries", func() {
				stack.Push(item{"viereins", "vierzwei"})
				len := stack.Len()
				Expect(len).To(BeNumerically("==", 3))
			})
			It("and top element is vier", func() {
				s := stack.Peek()
				Expect(s).To(Equal(item{"viereins", "vierzwei"}))
			})
			It("PeekFromTop 0 is vier", func() {
				s := stack.PeekFromTop(0)
				Expect(s).To(Equal(item{"viereins", "vierzwei"}))
			})
			It("PeekFromTop 1 is zwei", func() {
				s := stack.PeekFromTop(1)
				Expect(s).To(Equal(item{"zweieins", "zweizwei"}))
			})
			It("PeekFromBottom 0 is eins", func() {
				s := stack.PeekFromBottom(0)
				Expect(s).To(Equal(item{"einseins", "einszwei"}))
			})
			It("PeekFromBottom 1 is zwei", func() {
				s := stack.PeekFromBottom(1)
				Expect(s).To(Equal(item{"zweieins", "zweizwei"}))
			})
		})
	})

	Describe("empty stack tests", func() {
		Context("three string elements stack", func() {
			stack := collections.Stack{}

			It("has 0 entries", func() {
				len := stack.Len()
				Expect(len).To(BeNumerically("==", 0))
			})
			It("Peek is nil", func() {
				s := stack.Peek()
				Expect(s).To(BeNil())
			})
			It("Pop is nil", func() {
				s := stack.Pop()
				Expect(s).To(BeNil())
			})
			It("Peek is still nil", func() {
				s := stack.Peek()
				Expect(s).To(BeNil())
			})
			It("Pop is still nil", func() {
				s := stack.Pop()
				Expect(s).To(BeNil())
			})
			It("PeekFromTop 0 is nil", func() {
				s := stack.PeekFromTop(0)
				Expect(s).To(BeNil())
			})
			It("PeekFromTop 1 is nil", func() {
				s := stack.PeekFromTop(1)
				Expect(s).To(BeNil())
			})
			It("PeekFromBottom 0 is nil", func() {
				s := stack.PeekFromBottom(0)
				Expect(s).To(BeNil())
			})
			It("PeekFromBottom 1 is nil", func() {
				s := stack.PeekFromBottom(1)
				Expect(s).To(BeNil())
			})
			It("Push eins", func() {
				stack.Push("eins")
				s := stack.Peek()
				Expect(s).To(Equal("eins"))
				Expect(stack.Len()).To(BeNumerically("==", 1))
				s = stack.Pop()
				Expect(s).To(Equal("eins"))
				Expect(stack.Len()).To(BeNumerically("==", 0))
			})
			It("Peek is still still nil", func() {
				s := stack.Peek()
				Expect(s).To(BeNil())
			})
			It("Pop is still still nil", func() {
				s := stack.Pop()
				Expect(s).To(BeNil())
			})
		})

		Describe("stack slice tests", func() {
			Context("three string elements stack", func() {
				stack := collections.Stack{}
				stack.Push("eins")
				stack.Push("zwei")
				stack.Push("drei")
				slice := stack.Slice()

				It("slice has 3 entries", func() {
					len := len(slice)
					cap := cap(slice)
					Expect(len).To(BeNumerically("==", 3))
					Expect(cap).To(BeNumerically("==", 3))
				})
				It("slice entries are eins zwei und drei", func() {
					Expect(slice[0]).To(Equal("eins"))
					Expect(slice[1]).To(Equal("zwei"))
					Expect(slice[2]).To(Equal("drei"))
				})
				It("Stringer is using Slice", func() {
					s := fmt.Sprint(slice)
					Expect(s).To(Equal("[eins zwei drei]"))
				})
			})
		})

		Describe("stack byObject altering tests", func() {
			Context("three string elements stack", func() {
				stack := collections.Stack{}
				i1 := item{"einseins", "einszwei"}
				i2 := item{"zweieins", "zweizwei"}
				i3 := item{"dreieins", "dreizwei"}
				stack.Push(i1)
				stack.Push(i2)
				stack.Push(i3)

				It("altering top to view", func() {
					top := stack.Peek().(item)
					top.eins = "altered"

					Expect(top).To(Equal(item{"altered", "dreizwei"}))
					Expect(i3).To(Equal(item{"dreieins", "dreizwei"}))
					Expect(stack.Peek()).To(Equal(item{"dreieins", "dreizwei"}))
					Expect(i3).To(Equal(item{"dreieins", "dreizwei"}))
					Expect(stack.Pop()).To(Equal(item{"dreieins", "dreizwei"}))
					Expect(i3).To(Equal(item{"dreieins", "dreizwei"}))
				})
			})
		})

		Describe("stack byRef altering tests", func() {
			Context("three string elements stack", func() {
				stack := collections.Stack{}
				i1 := item{"einseins", "einszwei"}
				i2 := item{"zweieins", "zweizwei"}
				i3 := item{"dreieins", "dreizwei"}
				stack.Push(&i1)
				stack.Push(&i2)
				stack.Push(&i3)

				It("altering top to view", func() {
					top := stack.Peek().(*item)
					top.eins = "altered"

					Expect(*top).To(Equal(item{"altered", "dreizwei"}))
					Expect(i3).To(Equal(item{"altered", "dreizwei"}))
					Expect(*stack.Peek().(*item)).To(Equal(item{"altered", "dreizwei"}))
					Expect(i3).To(Equal(item{"altered", "dreizwei"}))
					Expect(*stack.Pop().(*item)).To(Equal(item{"altered", "dreizwei"}))
					Expect(i3).To(Equal(item{"altered", "dreizwei"}))
				})
			})
		})
	})
})
